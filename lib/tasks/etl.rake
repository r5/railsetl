namespace :etl do

  desc "Import Data from CSV"
  task import: :environment do
    raise ArgumentError, "Directory path not present", caller unless ENV["dir_path"]
    raise ArgumentError, "Output directory path not present", caller unless ENV["out_dir_path"]
    raise ArgumentError, "Validation mappings path path not present", caller unless ENV["validation_mappings_path"]
    validation_mappings = (JSON.parse File.read(ENV["validation_mappings_path"])).with_indifferent_access
    validation_mappings.each do |mapping_name, validations|
      Etl::Import::Job.call(validations: validations, source_dir_path: File.join(ENV["dir_path"], mapping_name), out_dir_path: File.join(ENV["out_dir_path"], mapping_name) )
    end
  end

  desc "Export Data from DB"
  task export: :environment do
    raise ArgumentError, "Mappings not present", caller unless ENV["mappings"]
    raise ArgumentError, "Table Names not present", caller unless ENV["table_names"]
    raise ArgumentError, "Output directory path not present", caller unless ENV["out_dir_path"]
    mappings = (JSON.parse File.read(ENV["mappings"])).with_indifferent_access
    table_names = ENV["table_names"].split(",").map(&:strip)
    mappings.each do |mapping_name, mapping|
      Etl::Export::Job.call(mappings: mapping, table_names: table_names, mapping_name: mapping_name, out_dir_path: ENV["out_dir_path"])
    end
  end
end
