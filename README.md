# Rails ETL

## Setup

[How to Setup](https://bitbucket.org/r5/railsetl/src/master/SETUP.md)

## Problem

Build a basic ETL system for the following DB structure
[ETL WikiPedia](https://en.wikipedia.org/wiki/Extract,_transform,_load)

## DB structure

- companies
    - id (integer)
    - name (string)
- services
    - id (integer)
    - name (string)
- clients
    - id (integer)
    - name (string)
    - email (string)
- appointments
    - id (integer)
    - begin_at (datetime)
    - end_at (datetime)
    - client_id (integer)
- appointment_services
    - id (integer)
    - appointment_id (integer)
    - service_id (integer)


## Extract

- export data in CSV files (support multiple formats - see next step)

## Transform

- allow multiple formats
  - possible CSV definitions 1
    - services.csv (id,name)
    - clients.csv (id,name,email)
    - appointments.csv (id,begin_at,end_at,client_id)
    - appointment_services.csv (id,appointment_id,service_id)

  - possible CSV definitions 2
    - variations.csv (id,title)
    - customers.csv (id,first_name,last_name,email)
    - events.csv (id,start_time,end_time,customer_id)
    - events_variations.csv (id,event_id,variation_id)

## Load

- idempotent (no matter how many times you import the same file, data should remain in a valid state)
- track what chages were made
- track also if no changes were made (don't update data in case you don't need to)
- validate content received (e.g. in case you have an appointment which is referencing a client_id which was not previously imported, you need to track that)
- output a report containing everything that happened during the ETL (changes made / validation errors / items ignored since nothing changed / etc)

## Requirements

- RoR project (we expect to see data being imported / exported into a DB)
- project does NOT have to be production ready, but should be functional and demonstrate knowledge of best practices and good code patterns
- code should be maintainable and to be able to easily add more mappings in the Extract and Transform step

