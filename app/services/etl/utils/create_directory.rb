module Etl

  module Utils

    class CreateDirectory
      # Craete a directory if it doen't already exixts
      extend SmartInit
      initialize_with :path
      is_callable

      def call
        dirname = File.dirname(path)
        unless File.directory?(dirname)
          FileUtils.mkdir_p(dirname)
        end
      end

    end

  end

end
