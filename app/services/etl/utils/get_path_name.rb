module Etl

  module Utils

    class GetPathName
      # Get Pathname with or withour extension from ruby Pathname object
      extend SmartInit
      initialize_with :path, with_extension: false
      is_callable

      def call
        with_extension ? path.basename : path.basename(path.extname).to_s
      end

    end

  end

end
