module Etl

  module Utils

    class SourceFilePathnames
      # Get Pathnames of all files inside a direcotry
      extend SmartInit
      initialize_with :dir_path
      is_callable

      def call
        files = []
        Dir.foreach(dir_path) do |file_path|
          next if file_path == '.' or file_path == '..'
          files << Pathname.new(File.join(dir_path, file_path))
        end
        files
      end

    end

  end

end
