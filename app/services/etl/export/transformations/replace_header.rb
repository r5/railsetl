module Etl

  module Export

    module Transformations
      # Chhange original header name of CSV to new name
      class ReplaceHeader
        extend SmartInit
        initialize_with :header_replacements
        def process(row)
          header_replacements.each do |original_name, new_name|
            row[new_name.to_s] = row.delete(original_name.to_s)
          end
          row
        end

      end

    end

  end

end
