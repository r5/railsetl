module Etl

  module Export

    module Transformations
      # Get first_name and last_name from full_name
      class SplitIntoTwo
        extend SmartInit
        initialize_with :splitables
        def process(row)
          splitables.each do |original_key, new_keys|
            # Todo change this so it works for more than two words name
            row.delete(original_key.to_s).split(" ").each_with_index {|val, i| row[new_keys[i].to_s] = val}
          end
          row
        end

      end

    end

  end

end
