module Etl

  module Export

    class Job
      # ['services', 'clients', 'appointments', 'appointment_services']
      extend SmartInit
      initialize_with :mappings, :mapping_name, :table_names, :out_dir_path
      is_callable

      def call
        table_names.each do |table_name|
          kiba_job = job(mappings[table_name.to_sym], table_name, mapping_name, out_dir_path)
          Kiba.run(kiba_job)
        end
      end

      def job(mapping, table_name, mapping_name, out_dir_path)
        kiba_job = Kiba.parse do
          source Etl::Sources::DbSource, {table_name: table_name, selected_attributes: mapping[:selected_attributes]}
          transform Transformations::ReplaceHeader, {header_replacements: mapping[:transform_rules][:replace_header]} if Maybe(mapping)[:transform_rules][:replace_header].is_some?
          transform Transformations::SplitIntoTwo, {splitables: mapping[:transform_rules][:split_into_two]} if Maybe(mapping)[:transform_rules][:split_into_two].is_some?
          destination Etl::Destinations::FileDestination, {output_file: Rails.root.join(out_dir_path, mapping_name , Maybe(mapping)[:transform_rules][:output_file_name].or_else {"#{table_name}.csv"} )}
        end
        kiba_job
      end

    end
  end
end
