module Etl

  module Import

    class Job
      # ['services', 'clients', 'appointments', 'appointment_services']
      extend SmartInit
      initialize_with :validations, :source_dir_path, :out_dir_path
      is_callable
      @@statistics = {}

      def associated
        @associated ||= Etl::Validations::PreloadAssociated.call(validations: validations, source_paths: @source_file_pathnames)
      end

      def statistics
        @@statistics
      end

      def call
        @source_file_pathnames = Etl::Utils::SourceFilePathnames.call(dir_path: source_dir_path)
        @source_file_pathnames.each do |path|
          # Todo refactor getting the file name from path
          resource_name = Etl::Utils::GetPathName.call(path: path, with_extension: false)
          validation = validations[resource_name]
          kiba_job = job(path, validation, associated, resource_name, out_dir_path, statistics)
          Kiba.run(kiba_job)
        end
        ap statistics
      end

      def job(path, validation, associated, resource_name, out_dir_path, statistics)
        statistics[resource_name] = {rejected: 0, presence_error_count: 0, associated_error_count: 0, success_count: 0}
        kiba_job = Kiba.parse do
          output_file_path = Rails.root.join(out_dir_path, "#{resource_name}.csv" )
          source Etl::Sources::CSVSource, {file_path: path}
          transform Transformations::RejectAlreadyCreated, old_file_path: output_file_path, on_reject: -> { statistics[resource_name][:rejected] += 1} if File.exist?(output_file_path)
          transform Transformations::ValidatePresent, {validations: validation[:validations][:present], on_error: -> { statistics[resource_name][:presence_error_count] += 1}} if Maybe(validation)[:validations][:present].is_some?
          transform Transformations::ValidateAssociated, {validations: validation[:validations][:associated], associated: associated, on_error: -> { statistics[resource_name][:associated_error_count] += 1}} if Maybe(validation)[:validations][:associated].is_some?
          transform Transformations::Success, {on_success: -> { statistics[resource_name][:success_count] += 1 }}
          destination Etl::Destinations::FileDestination, {output_file: output_file_path, is_appended: true}

        end
        kiba_job
      end

    end
  end
end
