module Etl

  module Import

    module Transformations

      class RejectAlreadyCreated

        attr_reader :already_created_ids, :on_reject

        def initialize(old_file_path:, on_reject:)
          @already_created_ids = []
          @on_reject = on_reject
          CSV.open(old_file_path, headers: true, header_converters: :symbol) do |csv|
            csv.each do |row|
              @already_created_ids << row[0]
            end
          end
        end

        def process(row)
          rejected_count = 0
          unless already_created_ids.include? row[:id]
            row
          else
            @on_reject.call
            nil
          end
        end

      end

    end

  end

end
