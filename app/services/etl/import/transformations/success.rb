module Etl

  module Import

    module Transformations

      class Success
        extend SmartInit
        initialize_with :on_success

        def process(row)
          if row[:errors] && row[:errors].present?
            row[:success] = false
          else
            on_success.call
            row[:success] = true
          end
          row
        end

      end

    end

  end

end
