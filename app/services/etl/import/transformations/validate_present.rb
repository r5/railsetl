module Etl

  module Import

    module Transformations

      class ValidatePresent
        extend SmartInit
        initialize_with :validations, :on_error

        def process(row)
          presence_error_count = 0
          validations.each do |validate|
            (row[:errors] ||= []).push("#{validate} needs to be present") and on_error.call unless row[validate]
          end
          row
        end

      end

    end

  end

end
