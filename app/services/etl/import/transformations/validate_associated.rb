module Etl

  module Import

    module Transformations

      class ValidateAssociated
        extend SmartInit
        initialize_with :validations, :associated, :on_error

        def process(row)
          validations.each do |validate|
            (row[:errors] ||= []).push("associated #{validate} is not yet created") and on_error.call unless associated[validate].include?(row[validate])
          end
          row
        end

      end

    end

  end

end
