require 'csv'
module Etl

  module Destinations

    class FileDestination
      # Destination for Kiba.parse
      extend SmartInit
      initialize_with :output_file, is_appended: false
      def write(row)
        Etl::Utils::CreateDirectory.call(path: output_file)
        @headers_written = true if File.exists?(output_file) and is_appended
        @csv ||= CSV.open(output_file, is_appended ? 'a+' : 'w')
        unless @headers_written
          @headers_written = true
          @csv << row.keys
        end
        @csv << row.values
      end

      def close
        @csv.close if @csv
      end

    end

  end

end
