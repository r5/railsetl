require 'csv'
module Etl

  module Sources

    class CSVSource
      # Source for Kiba.parse
      extend SmartInit
      initialize_with :file_path
      def each
        CSV.open(file_path, headers: true, header_converters: :symbol) do |csv|
          csv.each do |row|
            yield(row.to_hash.with_indifferent_access)
          end
        end
      end

    end

  end

end
