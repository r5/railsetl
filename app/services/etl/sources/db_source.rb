module Etl

  module Sources

    class DbSource
      extend SmartInit
      initialize_with :table_name, :selected_attributes
      def each
        table_name.classify.constantize.select(selected_attributes).find_each do |row|
          yield(row.attributes.with_indifferent_access)
        end
      end

    end

  end

end
