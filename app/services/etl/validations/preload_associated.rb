require 'csv'

module Etl

  module Validations

    class PreloadAssociated
      # Preload associated IDs for validations
      extend SmartInit
      initialize_with :validations, :source_paths, loaded_ids: {}
      is_callable

      def call
        validations.each do |source, validation|
          next unless Maybe(validation)[:validations][:associated].is_some?
          validation[:validations][:associated].each do |associated|
            # Get name of associated source so the file path could be found
            associated_socurce_name = source_name_from(associated)
            # Get the path of the assciated source file
            associated_socurce_path = find_associated_source_path(associated_socurce_name)
            # Load ids from the file for validations
            load_ids(associated_socurce_path, associated)
          end
        end
        loaded_ids
      end

      def find_associated_source_path(source_name)
        source_paths.find do |path|
           source_name == Etl::Utils::GetPathName.call(path: path, with_extension: false)
        end
      end

      def source_name_from(id)
        id.to_s.sub("_id", '').pluralize
      end

      def load_ids(associated_socurce_path, associated)
        loaded_ids[associated] ||= []
        return unless associated_socurce_path
        CSV.open(associated_socurce_path, headers: true, header_converters: :symbol) do |csv|
          csv.each do |row|
            loaded_ids[associated] << row[0]
          end
        end
      end


    end

  end

end
