# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Company.create([{name: 'Eros'}, {name: 'Retl'}, {name: 'Times'}])

Service.create([{name: 'Massage'}, {name: 'Hair Cut'}, {name: 'Pedicure'}])

Client.create([{name: 'Emanuel Nedelcu', email: 'emanuel@gmail.com'}, {name: 'Ehsanul Hoque', email: 'ehsan@gmail.com'}, {name: 'Vipin Anderson', email: 'ethan@gmail.com'}])

Appointment.create([{begin_at: 5.hours.from_now, end_at: 6.hours.from_now, client_id: 1 }, {begin_at: 5.hours.from_now, end_at: 6.hours.from_now, client_id: 2 }, {begin_at: 5.hours.from_now , end_at: 6.hours.from_now, client_id: 2 }])

AppointmentService.create([{appointment_id: 1, service_id: 1}, {appointment_id: 2, service_id: 2}, {appointment_id: 3, service_id: 3}])
