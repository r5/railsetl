# Rails ETL

## Installation

1. Install Ruby Version `2.5.4`
2. Install bundle
3. run `rake db:setup`



### Running commands

1. Extract

  ```bash
  bundle exec rails etl:export --trace mappings="~/railsetl/tmp/storage/exports/mapping1.json" out_dir_path="~/railsetl/tmp/storage/exports" table_names="services,clients,appointments,appointment_services"
  ```

2. Load

  ```bash
  bundle exec rails etl:import --trace dir_path="~/railsetl/tmp/storage/exports/" out_dir_path="/Users/emanuel/mytime/tmp/storage/imports/reports" validation_mappings_path="~/railsetl/tmp/storage/imports/mapping1.json"
  ```

### Improvements

1. Validation mappings for `import` should be optional
2. Tests need to be added
3. Splitting full_name can be improved

### More Information

1. The application heavily relies on [Kiba-etl](https://github.com/thbar/kiba) and [smart_init](https://github.com/pawurb/smart_init)
2. The above rake commands Import and Export kiba jobs. The code can be found in `services/import/job.rb` and `services/export/job.rb`
3. Please refer `tmp/storage/exports/mapping1.json` and `tmp/storage/export/mapping2.json` for different export definitions
4. Please refer `tmp/storage/importss/mapping1.json` and `tmp/storage/imports/mapping2.json` for enabling validations while importing
